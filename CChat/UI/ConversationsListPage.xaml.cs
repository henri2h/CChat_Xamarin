﻿using CChat.Objects;
using CChat.UI.Accounts;
using CChat.UI.CustomMessageView;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Plugin.Toasts;
using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CChat.UI
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConversationsListPage : ContentPage
    {
        public ObservableCollection<ConversationListItem> ConvList { get; set; }
        public ConversationsListPage()
        {
            InitializeComponent();
            ConvList = new ObservableCollection<ConversationListItem>();
            BindingContext = this;
        }

        public void loadConversations()
        {
            ConvList.Clear();
            foreach (ConversationListItem c in App.client.ConversationList)
            {
                ConvList.Add(c);
            }

        }

        private async void ContentPage_Appearing(object sender, System.EventArgs e)
        {
            try
            {
                App.client = new ClientConnector.ClientAccess(App.AccountSelected); // open the connector

                loadConversations();
                await App.client.UpdateConversationListAsync();
                loadConversations();
            }
            catch
            {

                App.Current.MainPage = new NavigationPage(new AccountsSelectionPage());

                await DisplayAlert("We had trouble connecting using this account", "Please check if you are connected to the internet and if the account settings are correct", "Cancel");
                var notificator = DependencyService.Get<IToastNotificator>();
                var options = new NotificationOptions()
                {
                    Title = "We had trouble connecting using this account",
                    Description = "Please check if you are connected to the internet and if the account settings are correct"
                };

                var result = await notificator.Notify(options);
                Analytics.TrackEvent("Could not connect with this account");
                System.Diagnostics.Debug.WriteLine("We had trouble connecting using this account");

               
            }

           
        }
        

        private async void CollectionView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ConversationListItem cli = (ConversationListItem)e.CurrentSelection[0]; // because it can return multiple elements

            if (cli != null)
            {
                await Navigation.PushAsync(new ConversationDetailView(cli));
            }
        }

        private void Button_AddConversation(object sender, System.EventArgs e)
        {
            try
            {
                App.client.CreateConversation("Conv name", new string[] { "henri2h", "quentin", "marin" });
            }
            catch(Exception ex)
            {
                Crashes.TrackError(ex);
                System.Diagnostics.Debug.WriteLine("Could not add conversation");
            }
        }

        private async void MenuItemAccountSetting_ClickedAsync(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AccountInformationsPage());
        }
    }
}