﻿using CChat.Objects;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CChat.UI
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class First_run : ContentPage
    {
        public First_run()
        {
            InitializeComponent();
        }

        private void btAddAccount(object sender, EventArgs e)
        {
            App.Current.MainPage = new NavigationPage(new AccountsSelectionPage());
        }

        private async void ContentPage_AppearingAsync(object sender, EventArgs e)
        {
            Account ac = await App.DatabaseAccess.GetDefaultAccountAsync();
            if (ac != null)
            {

                App.AccountSelected = ac; // create client access
                App.Current.MainPage = new NavigationPage(new ConversationsListPage());
            }
        }
    }
}