﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CChat.UI.Accounts
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AccountInformationsPage : ContentPage
	{
		public AccountInformationsPage ()
		{
			InitializeComponent ();
            Lb_AccountName.Text = App.AccountSelected.UserName;
		}

        private async void BtChangeUser_ClickedAsync(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AccountsSelectionPage());
        }
    }
}