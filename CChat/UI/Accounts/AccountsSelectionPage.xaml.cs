﻿using CChat.Objects;
using CChat.UI.Accounts;
using Microsoft.AppCenter.Crashes;
using Plugin.Toasts;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CChat.UI
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AccountsSelectionPage : ContentPage
    {
        public ObservableCollection<Account> Accounts { get; set; }

        public AccountsSelectionPage()
        {
            System.Diagnostics.Debug.WriteLine("LAUNCH");
            Accounts = new ObservableCollection<Account>();
            InitializeComponent();

            Stack_NoUser.IsVisible = false;

            System.Diagnostics.Debug.WriteLine("Account selection loaded");
            this.BindingContext = this;

        }

        private async void ListView_ItemTappedAsync(object sender, ItemTappedEventArgs e)
        {
            Account ac = (Account)e.Item;
            if (ac != null)
            {
                App.AccountSelected = ac;
                ac.Default = true;


                // save and update the account
                await App.DatabaseAccess.SetAccountAsDefaultAsync(ac);
                ac = await App.DatabaseAccess.GetDefaultAccountAsync();

                if (ac != null)
                {
                    System.Diagnostics.Debug.WriteLine("Going to navigate to ", ac.UserName);
                    ChangePageAsync(ac);
                }
            }

        }

        private void ChangePageAsync(Account ac)
        {
            try
            {
                App.Current.MainPage = new NavigationPage(new ConversationsListPage());
                //App.Current.MainPage = new NavigationPage(new AccountHomePage(ac));
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex);
            }
        }

        private async void ShowNotificationAsync()
        {

            var notificator = DependencyService.Get<IToastNotificator>();
            var options = new NotificationOptions()
            {
                Title = "Title",
                Description = "Description"
            };

            var result = await notificator.Notify(options);
        }

        private async void BtAdduser_ClickedAsync(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AddAccountPage());
        }


        public async Task LoadAccountsAsync()
        {
            Accounts.Clear();
            foreach (Account ac in await App.DatabaseAccess.GetAccountsAsync())
            {
                Accounts.Add(ac);
            }
        }


        /// <summary>
        /// Load the user list form the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void ContentPage_AppearingAsync(object sender, EventArgs e)
        {
            await LoadAccountsAsync();
        }

        private async void MenuItem_ModifyAccountAsync_Clicked(object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);
            string ID = mi.CommandParameter.ToString();

            try
            {
                int ID_int = int.Parse(ID);
                Account ac = await App.DatabaseAccess.GetAccountAsync(ID_int);

                if (ac != null)
                {
                    await Navigation.PushAsync(new AddAccountPage(ac));
                }

                await LoadAccountsAsync();
            }
            catch { await DisplayAlert("Could not update", "Retry", "OK"); }
        }

        private async void MenuItem_DeleteAccountAsync_Clicked(object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);
            string ID = mi.CommandParameter.ToString();

            try
            {
                int ID_int = int.Parse(ID);
                Account ac = await App.DatabaseAccess.GetAccountAsync(ID_int);

                if (ac != null)
                {
                    await App.DatabaseAccess.DeleteAccountAsync(ac);
                }
                await LoadAccountsAsync();
            }
            catch { await DisplayAlert("Could not update", "Retry", "OK"); }
        }
    }
}