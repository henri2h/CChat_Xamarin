﻿using CChat.Objects;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CChat.UI.Accounts
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddAccountPage : ContentPage
    {
        Account ac;
        /// <summary>
        /// Add a new user
        /// </summary>
        public AddAccountPage()
        {
            InitializeComponent();
            ac = new Account();
        }

        /// <summary>
        /// Update an existing user
        /// </summary>
        /// <param name="ac"></param>
        public AddAccountPage(Account ac)
        {
            InitializeComponent();
            Title = "Modify " + ac.UserName;
            this.ac = ac;
            btAdduser.Text = "Modify account";

            // load data

            En_AccountHost.Text = ac.Host;
            En_AccountName.Text = ac.AccountName;
            En_AccountUsername.Text = ac.UserName;
            En_AccountPort.Text = ac.Port.ToString();

        }

        /// <summary>
        /// Add the user and set it as default
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void BtAdduser_ClickedAsync(object sender, EventArgs e)
        {
            try
            {

                if (En_AccountName.Text != "" && En_AccountUsername.Text != "" && En_AccountPort.Text != "" && En_AccountHost.Text != "")
                {
                    ac.AccountName = En_AccountName.Text;
                    ac.Default = true;
                    ac.UserName = En_AccountUsername.Text;
                    ac.Port = int.Parse(En_AccountPort.Text);
                    ac.Host = En_AccountHost.Text;

                    await App.DatabaseAccess.AddDefaultAccountAsync(ac);
                    ac = await App.DatabaseAccess.GetDefaultAccountAsync(); // get the new account in order to update the user ID (used in data storage)

                    App.AccountSelected = ac;
                    App.Current.MainPage = new NavigationPage(new ConversationsListPage());
                }
            }
            catch
            {
                System.Diagnostics.Debug.WriteLine("Could not add user...");
            }
        }
    }
}