﻿using CChat.ClientConnector;
using CChat.Code.Data;
using CChat.Objects;
using CChat.UI;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Microsoft.AppCenter.Distribute;
using Plugin.Toasts;
using System.Collections.Generic;
using System.Timers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace CChat
{
    public partial class App : Application
    {
        Timer updateConversationTimer;
        public App()
        {
            AppCenter.Start("android=ebad506b-abb3-4f23-9dd1-15a0f2e4a210;uwp=e09f520d-d3fd-4abc-92c7-eeefa2a6981b", typeof(Analytics), typeof(Crashes), typeof(Distribute));

            InitializeComponent();

            System.Diagnostics.Debug.WriteLine("Started");
            Analytics.TrackEvent("Launched");

            DatabaseAccess = new Database();

            DetectIfFirstRun();
        }

        public async void DetectIfFirstRun()
        {
            Account x = await DatabaseAccess.GetDefaultAccountAsync();
            if (x == null)
            {
                // is first run

                MainPage = new First_run();
            }
            else
            {
                AccountSelected = x;

                // launch timer
                updateConversationTimer = new System.Timers.Timer();
                updateConversationTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent_UpdateConversation);
                updateConversationTimer.Interval = 5000;
                updateConversationTimer.Enabled = true;


                MainPage = new NavigationPage(new ConversationsListPage());
            }

        }



        // Specify what you want to happen when the Elapsed event is raised.
        private async void OnTimedEvent_UpdateConversation(object source, ElapsedEventArgs e)
        {
            List<ConversationMessage> NewMessages = await client.FetchFromServerAsync();
            if (NewMessages.Count > 0)
            {
                foreach(ConversationMessage conversationMessage in NewMessages)
                {
                    Notification(client.GetConversationName(conversationMessage.ConvID), conversationMessage.Username + " : " + conversationMessage.Content);
                }
                Track("New message recieved");
            }
        }

        public async void Notification(string title, string description)
        {

            var notificator = DependencyService.Get<IToastNotificator>();
            var options = new NotificationOptions()
            {
                Title = title,
                Description = description
            };
            if (notificator != null)
            {
                var result = await notificator.Notify(options);
            }
            else
            {
                Track("Cannot notify");
            }
        }

        public void Track(string text)
        {

            System.Diagnostics.Debug.WriteLine(text);
            Analytics.TrackEvent(text);
        }

        protected override void OnStart()
        {
            // Handle when your app starts
            Analytics.TrackEvent("Started");
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
            Analytics.TrackEvent("Sleep");
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
            Analytics.TrackEvent("Resume");
        }

        public static ClientAccess client { get; internal set; }
        public static Database DatabaseAccess { get; internal set; }
        public static Account AccountSelected { get; internal set; }
    }
}
