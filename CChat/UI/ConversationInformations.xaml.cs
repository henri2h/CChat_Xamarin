﻿using CChat.Objects;
using System;
using System.Collections.ObjectModel;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CChat.UI
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConversationInformations : ContentPage
    {
        public ObservableCollection<User> Users { get; set; }

        public ConversationListItem ConvDetail { get; set; }

        public ConversationInformations(ConversationListItem convDetail)
        {
            InitializeComponent();

            this.Title = convDetail.ConvName;
            ConvDetail = convDetail;

            // load users
            Users = new ObservableCollection<User>(App.client.GetUsersInConv(convDetail.ConvID));
        }

        private void Button_ChangeConvName_Clicked(object sender, EventArgs e)
        {
            ConvDetail.ConvName = En_ConvName.Text;
            App.client.SetConversationName(ConvDetail);
        }

        private void Button_AddUser(object sender, EventArgs e)
        {
            if (En_AddUser.Text != "")
            {
                App.client.AddUser(ConvDetail.ConvID, En_AddUser.Text);


                // add user to UI, but should refresh list comming from server
                User u = new User
                {
                    username = En_AddUser.Text
                };
                Users.Add(u);

                En_AddUser.Text = "";
            }
            
        }
    }
}