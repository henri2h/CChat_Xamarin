﻿using CChat.Objects;
using Xamarin.Forms;

namespace CChat.UI.CustomMessageView
{
    public class TemplateSelector_MessageView : DataTemplateSelector
    {
        public DataTemplate OutGoingMessagesTextTemplate { get; set; }
        public DataTemplate OutGoingMessagesImageTemplate { get; set; }
        public DataTemplate InGoingMessagesTextTemplate { get; set; }

        public DataTemplate InGoingMessagesImageTemplate { get; set; }
        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            ConversationMessage cm = (ConversationMessage)item;
            if (cm.Username == App.AccountSelected.UserName)
            {
                if (cm.DataType == dataType.text)
                {
                    return OutGoingMessagesTextTemplate; // the user send a message
                }
                else return OutGoingMessagesImageTemplate;
            }
            else
            {
                if (cm.DataType == dataType.text)
                {
                    return InGoingMessagesTextTemplate; // the user recieve a message
                }
                else return InGoingMessagesImageTemplate;
            } 
        }
    }
}
