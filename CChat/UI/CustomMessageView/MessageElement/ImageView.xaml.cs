﻿using CChat.Objects;
using Microsoft.AppCenter.Crashes;
using System;
using System.IO;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CChat.UI.CustomMessageView.MessageElement
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ImageView : ContentView
    {
        public ImageView()
        {
            InitializeComponent();
        }

        public static BindableProperty DataTypeProperty = BindableProperty.Create(
                                                        propertyName: "DataType",
                                                        returnType: typeof(dataType),
                                                        declaringType: typeof(dataType),
                                                        defaultValue: dataType.text, // text by default
                                                        defaultBindingMode: BindingMode.TwoWay,
                                                        propertyChanged: DataTypeChangedAsync);

        public static BindableProperty MessageContentProperty = BindableProperty.Create(
                                                       propertyName: "MessageContent",
                                                       returnType: typeof(string),
                                                       declaringType: typeof(string),
                                                       defaultValue: "", // text by default
                                                       defaultBindingMode: BindingMode.TwoWay,
                                                       propertyChanged: MessageContentChangedAsync);

        public static BindableProperty ConvIDProperty = BindableProperty.Create(
                                                       propertyName: "ConvID",
                                                       returnType: typeof(string),
                                                       declaringType: typeof(string),
                                                       defaultValue: "", // text by default
                                                       defaultBindingMode: BindingMode.TwoWay,
                                                       propertyChanged: ConvIDChangedAsync);

        public string ConvID
        {
            get { return (string)base.GetValue(ConvIDProperty); }
            set
            {
                base.SetValue(ConvIDProperty, value);
                System.Diagnostics.Debug.WriteLine(value);
            }
        }
        public string MessageContent
        {
            get { return (string)GetValue(MessageContentProperty); }
            set
            {
                base.SetValue(ContentProperty, value);
                System.Diagnostics.Debug.WriteLine(value);
            }
        }
        public dataType DataType
        {
            get { return (dataType)GetValue(DataTypeProperty); }
            set
            {
                base.SetValue(DataTypeProperty, value);
                System.Diagnostics.Debug.WriteLine(value);
            }
        }

        bool MessageContentChanged = false;
        bool ConvIDChanged = false;
        bool DataTypeChanged = false;
        private static async void MessageContentChangedAsync(BindableObject bindable, object newValue, object oldValue)
        {
            var control = (ImageView)bindable;
            if (control.MessageContent != "")
            {
                control.MessageContentChanged = true;
                if (control.ConvIDChanged && control.DataTypeChanged)
                {
                    await control.UpdateConvAsync();
                }
            }
        }


        private static async void ConvIDChangedAsync(BindableObject bindable, object newValue, object oldValue)
        {
            var control = (ImageView)bindable;

            if (control.ConvID != "")
            {
                control.ConvIDChanged = true;
                if (control.MessageContentChanged && control.DataTypeChanged)
                {
                    await control.UpdateConvAsync();
                }
            }
        }

        private static async void DataTypeChangedAsync(BindableObject bindable, object newValue, object oldValue)
        {
            var control = (ImageView)bindable;
            control.DataTypeChanged = true;
            if (control.MessageContentChanged && control.ConvIDChanged)
            {
                await control.UpdateConvAsync();
            }
        }


        private async System.Threading.Tasks.Task UpdateConvAsync()
        {

            //      control.Image_UI.Source = ImageSource.FromFile(newValue.ToString());

            if (ConvID != "" && MessageContent != "")
            {
                try
                {
                    if (DataType == dataType.text)
                    {

                        ActivityIndicator_UI.IsVisible = false;
                        ActivityIndicator_UI.IsRunning = false;

                        Lb_text.IsEnabled = true;
                        Lb_text.IsVisible = true;
                        Lb_text.Text = MessageContent;
                    }

                    else

                    if (DataType == dataType.image)
                    {
                        Image_UI.IsEnabled = true;
                        Lb_text.IsEnabled = true;
                        Lb_text.IsVisible = true;
                        Lb_text.Text = MessageContent;

                        byte[] image = await App.client.GetImageAsync(ConvID, MessageContent);

                        System.Diagnostics.Debug.WriteLine(image.Length);
                        Image_UI.Source = ImageSource.FromStream(() => new MemoryStream(image));

                        ActivityIndicator_UI.IsVisible = false;
                        ActivityIndicator_UI.IsRunning = false;

                        Image_UI.IsVisible = true;
                        Lb_text.IsVisible = false;
                        Lb_text.IsEnabled = false;
                    }

                }
                catch (Exception ex)
                {
                    Crashes.TrackError(ex);
                    System.Diagnostics.Debug.WriteLine("Error while download the image");
                }
            }
        }
    }
}