﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CChat.UI.CustomMessageView.MessageElement
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Text : ContentView
	{
		public Text ()
		{
			InitializeComponent ();
		}
	}
}