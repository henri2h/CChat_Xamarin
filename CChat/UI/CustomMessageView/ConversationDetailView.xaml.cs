﻿
using CChat.Objects;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Plugin.FilePicker;
using Plugin.FilePicker.Abstractions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Timers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CChat.UI.CustomMessageView
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConversationDetailView : ContentPage
    {
        private static System.Timers.Timer aTimer;

        private bool stillNoUserInteraction = true; // deteck if we can continue to update the ui
        private bool sendingMessage = false;
        private bool updatingUI = false;

        public string convID { get; set; }
        public ConversationListItem ConvDetails { get; set; }

        public ObservableCollection<ConversationMessage> ConversationMessages { get; set; }

        public ConversationDetailView(ConversationListItem cid)
        {
            ConversationMessages = new ObservableCollection<ConversationMessage>();

            InitializeComponent();

            // update convID
            convID = cid.ConvID;
            ConvDetails = cid;
            BindingContext = this;
            SetTimer();
        }

        private void SetTimer()
        {
            // Create a timer with a two second interval.
            aTimer = new System.Timers.Timer(2000);
            // Hook up the Elapsed event for the timer. 
            aTimer.Elapsed += OnTimedEventAsync;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }


        private async void OnTimedEventAsync(Object source, ElapsedEventArgs e)
        {
            // should we update
            await App.client.UpdateConversationAsync(convID);
            if (App.client.GetConversation(convID).Messages[App.client.GetConversation(convID).Messages.Count - 1].MessageID != ConversationMessages[ConversationMessages.Count - 1].MessageID)
            {
                DisplayMessages();
            }
        }

        private async void ContentPage_Appearing(object sender, EventArgs e)
        {
            if (stillNoUserInteraction) DisplayMessages();
            await App.client.UpdateConversationAsync(convID);
            if (stillNoUserInteraction) DisplayMessages();
        }

        private void DisplayMessages()
        {

            updatingUI = true;
            try
            {
                ConversationMessages.Clear();

                foreach (ConversationMessage cm in App.client.GetConversation(convID).Messages)
                {
                    cm.ConvID = convID; // some messages seems to have no conv
                    ConversationMessages.Add(cm); // Waring can thow exception
                }

                if (ConversationMessages.Count > 0) CollectionView.ScrollTo(ConversationMessages[ConversationMessages.Count - 1]);
            }
            catch (Exception ex)
            {
                // for some reason I have got an expetion when modifying ConversationMessagesList
                Crashes.TrackError(ex);
                Analytics.TrackEvent("Error while reloading conversation messages view data");         
            updatingUI = false;
            }
         }


        private void Button_Send_Clicked(object sender, EventArgs e)
        {
            SendMessage();
        }

        List<byte[]> images = new List<byte[]>();

        private async void Button_AddImage_ClickedAsync(object sender, EventArgs e)
        {
            try
            {
                FileData fileData = await CrossFilePicker.Current.PickFile(new string[] { ".jpg", ".png" });
                if (fileData == null)
                    return; // user canceled file picking

                string fileName = fileData.FileName;
                images.Add(fileData.DataArray);

                System.Console.WriteLine("File name chosen: " + fileName);


                Image image = new Image
                {
                    Source = ImageSource.FromStream(() => new MemoryStream(fileData.DataArray)),
                    WidthRequest = 75
                };
                StackImages.Children.Add(image);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine("Exception choosing file: " + ex.ToString());
            }
        }

        /// <summary>
        /// Get the text in the entry and send the message
        /// </summary>
        private async void SendMessage()
        {
            try
            {
                stillNoUserInteraction = false; // we are going to modify the message list
                if (updatingUI)
                {
                    System.Diagnostics.Debug.WriteLine("UI is updating");
                }
                foreach (byte[] image in images)
                {
                    await App.client.SendImage(App.AccountSelected.UserName, convID, image);
                }

                if (EMessage.Text != "")
                {
                    ConversationMessage tcm = new ConversationMessage(App.AccountSelected.UserName, convID);
                    tcm.Content = EMessage.Text;
                    await App.client.SendConversationMessage(tcm);
                    EMessage.Text = "";


                    // update conv
                    await App.client.UpdateConversationAsync(convID);
                    DisplayMessages();
                }
                else
                {
                    Analytics.TrackEvent("Could send without conversation selected");
                }


                StackImages.Children.Clear();
                images.Clear();
            }
            catch (Exception ex)
            {
                await DisplayAlert("An error happend", "Could not send conversation mesage", "Cancel");
                Crashes.TrackError(ex);

                Analytics.TrackEvent("Couldn't send conversation message");
                System.Diagnostics.Debug.WriteLine("Could not send message");
            }
        }

        private async void MenuItem_ConversationSettings(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ConversationInformations(ConvDetails));
        }

    }
}