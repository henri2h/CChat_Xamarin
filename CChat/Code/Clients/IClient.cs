﻿using CChat.Objects;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CChat.Clients
{
    public interface IClient
    {
        // Globals
        string ProtocolVersion { get; }


        /// <summary>
        /// User connected
        /// </summary>
        User CurrentUser { get; }

        /// <summary>
        /// Indicate if the client is connected and logged in
        /// </summary>
        bool Connected { get; }

        bool Created { get; set; }
        bool Creating { get; set; }
        

        // test the connection, usefull ?
        Task<bool> TestConnection();
         
        /// <summary>
        /// Return true if the client is connected and the server available
        /// TODO : In the future, check if the token is still valable (here or in the server)
        /// </summary>
        /// <returns></returns>
        Task<bool> CanUserUseConnection(); 


        // Globals
        ConnectionMethod GetConnectionMethod();


        /// <summary>
        /// List conversations and names
        /// </summary>
        /// <returns></returns>
        Task<ConversationListItem[]> ListConversations();
        

        int GetNewMessagesCount(string convID, string lastMessageID);
        

        /// <summary>
        /// Send a message to the conversation
        /// </summary>
        /// <param name="tcpConvMess"></param>
        /// <returns></returns>
        Task SendConversationMessage(ConversationMessage tcpConvMess);

        Task<byte[]> GetConversationImage(string fileName, string convID);

        /// <summary>
        /// Create a new conversation
        /// </summary>
        /// <param name="convName"></param>
        /// <param name="users"></param>
        /// <returns></returns>
        Task CreateConversation(string convName, string[] users);


        /// <summary>
        /// Add a user to a specified conversation
        /// </summary>
        /// <param name="convID"></param>
        /// <param name="users_to_add"></param>
        void AddUser(string convID, string users_to_add);

        /// <summary>
        /// Download a conversation
        /// </summary>
        /// <param name="convID"></param>
        /// <returns></returns>
        Task<Conversation> GetConversation(string convID);

        Task ChangeConversationName(string convID, string convName);
        Task sendImage(string userName, string convID, byte[] image);
    }
}
