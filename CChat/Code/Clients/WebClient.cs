﻿using CChat.Objects;

using System;
using System.Threading.Tasks;

namespace CChat.Clients
{
    public class WebClient : IClient
    {
        private HttpClient client;
        private string token;
        public User currentUser = new User();
        // not used
        public bool created = true;

        // this has to be dynamicaly set
        public bool logedIn = true;
        public bool creating = false;
        public bool isClientLogedIn;



        string IClient.ProtocolVersion { get { return "1.1.0"; } }
        User IClient.CurrentUser { get { return currentUser; } }
        bool IClient.Connected { get { return isClientLogedIn; } }
        bool IClient.Created { get { return created; } set { created = value; } }
        bool IClient.Creating { get { return creating; } set { creating = value; } }


        public WebClient(string username, Uri baseUri)
        {
            currentUser = new User();
            this.client = new HttpClient(username, "", baseUri);
            try
            {
                //TODO : check if we are connected to the server
                isClientLogedIn = true;
                // maybe client is connected
            }
            catch
            {
                isClientLogedIn = false;
            }
        }


        async Task IClient.CreateConversation(string convName, string[] users)
        {
            string usersJSON = Newtonsoft.Json.JsonConvert.SerializeObject(users);
            await client.POSTrequestStringAsync("/Conversation/CreateConversation", usersJSON);
        }


        async Task<Conversation> IClient.GetConversation(string convID)
        {
            ConversationListItem cl = new ConversationListItem
            {
                ConvID = convID,
                ConvName = ""
            };
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(cl);
            string conversation = await client.POSTrequestStringAsync("/Conversation/GetConversation", json);

            if (conversation == "KO") { return null; }

            return Newtonsoft.Json.JsonConvert.DeserializeObject<Conversation>(conversation);
        }

        async Task<byte[]> IClient.GetConversationImage(string fileName, string convID)
        {

            GetConversationImageElement cl = new GetConversationImageElement
            {
                convID = convID,
                fileName = fileName
            };

            string json = Newtonsoft.Json.JsonConvert.SerializeObject(cl);
            byte[] data = await client.POSTrequestBytesAsync("/Conversation/GetPicture", json);
            return data;

        }

        async Task IClient.ChangeConversationName(string convName, string convID)
        {
            ConversationListItem cl = new ConversationListItem
            {
                ConvID = convID,
                ConvName = convName
            };
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(cl);
            string text = await client.POSTrequestStringAsync("/Conversation/SetConversationName", json);
            if (text != "OK") { throw new Exception("Conversation name not changed"); }
        }

        async Task<ConversationListItem[]> IClient.ListConversations()
        {
            string text = await client.POSTrequestStringAsync("/Conversation/ListConversations", "");
            ConversationListItem[] convIDS = Newtonsoft.Json.JsonConvert.DeserializeObject<ConversationListItem[]>(text); // should be an array because this data should not be modified
            return convIDS;
        }

        async Task IClient.SendConversationMessage(ConversationMessage nMess)
        {
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(nMess);
            string success = await client.POSTrequestStringAsync("/Conversation/SendConversationMessage", json);
            if (success != "OK") { throw new Exception("Message not sended"); }
        }

        async Task<bool> IClient.TestConnection()
        {
            return await TestServerConnection();
        }


        ConnectionMethod IClient.GetConnectionMethod()
        {
            return ConnectionMethod.password;
        }




        public int GetNewMessagesCount(string convID, string lastMessageID)
        {
            throw new NotImplementedException();
        }

        public Task CreateConversation(string convName, string[] users)
        {
            throw new NotImplementedException();
        }

        public async Task ChangeConversationName(string convID, string convName)
        {
            ConversationListItem li = new ConversationListItem()
            {
                ConvID = convID,
                ConvName = convName
            };

            string json = Newtonsoft.Json.JsonConvert.SerializeObject(li);
            string success = await client.POSTrequestStringAsync("/Conversation/SetConversationName", json);
            if (success != "OK") { throw new Exception("Could not change conversation name"); }
        }

        async Task<bool> IClient.CanUserUseConnection()
        {
            if (isClientLogedIn && await TestServerConnection())
            {
                return true;
            }
            else return false;
        }

        int IClient.GetNewMessagesCount(string convID, string lastMessageID)
        {
            throw new NotImplementedException();
        }

        private async Task<bool> TestServerConnection()
        {
            try
            {
                string success = await client.POSTrequestStringAsync("/isUp", "");
                if (success == "OK")
                {
                    return true;
                }
                else if (success == "KO")
                {
                    return false;
                }
                else
                {
                    // Here is an issue, invalid response
                    System.Diagnostics.Debug.WriteLine("Could not connect");
                }
                return false;
            }

            catch
            {
                return false;
            }

        }

        public void AddUser(string convID, string users_to_add)
        {
            throw new NotImplementedException();
        }

        public async Task sendImage(string userName, string convID, byte[] image)
        {
            string success = await client.HTTP_POSTImageAsync("/Conversation/SendPicture", convID, image);
        }

        private class GetConversationImageElement // to review
        {
            public string convID { get; set; }
            public string fileName { get; set; }
        }
    }
}
