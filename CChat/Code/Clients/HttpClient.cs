﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace CChat.Clients
{
    /// <summary>
    /// class used by the web client class
    /// </summary>
    public class HttpClient
    {
        string username;
        string token;
        Uri baseUri;

        public HttpClient(string username, string token, Uri baseUri)
        {
            this.username = username;
            this.token = token;
            this.baseUri = baseUri;
        }
        
        public async void GetRequest()
        {
            Uri geturi = new Uri("http://api.openweathermap.org/data/2.5/weather?q=London"); //replace your url
            System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
            System.Net.Http.HttpResponseMessage responseGet = await client.GetAsync(geturi);
            string response = await responseGet.Content.ReadAsStringAsync();
        }
        public async Task<string> HTTP_POSTAsync(string path, string Data)
        {
            UriBuilder ub = new UriBuilder(baseUri)
            {
                Path = path
            };
            Uri requestUri = ub.Uri;

            string Out = String.Empty;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requestUri);
            request.Method = "POST";
            request.ContinueTimeout = 10000;
            request.ContentType = "application/json";
            request.Accept = "application/json";
   
            if (username != "")
            {
                request.Headers["x-token"] = token;
                request.Headers["x-username"] = username;
            }

            byte[] sentData = Encoding.UTF8.GetBytes(Data);
            // = sentData.Length;

            using (System.IO.Stream sendStream = await request.GetRequestStreamAsync())
            {
                sendStream.Write(sentData, 0, sentData.Length);
                sendStream.Dispose();
            }

            try
            {
                WebResponse w = await request.GetResponseAsync();
                Stream responseStream = w.GetResponseStream();

                StreamReader reader = new StreamReader(responseStream);
                Out = reader.ReadToEnd();
            }
            catch (ArgumentException ex)
            {
                Out = string.Format("HTTP_ERROR :: The second HttpWebRequest object has raised an Argument Exception as 'Connection' Property is set to 'Close' :: {0}", ex.Message);
            }
            catch (WebException ex)
            {

                Out = string.Format("HTTP_ERROR :: WebException raised! :: {0}", ex.Message);
                Out += Environment.NewLine + "Content : ";

                Stream ReceiveStreame = ex.Response.GetResponseStream();
                using (StreamReader sr = new StreamReader(ReceiveStreame, Encoding.UTF8))
                {
                    Char[] read = new Char[256];
                    int count = sr.Read(read, 0, 256);

                    while (count > 0)
                    {
                        String str = new String(read, 0, count);
                        Out += str;
                        count = sr.Read(read, 0, 256);

                    }
                }

            }
            catch (Exception ex)
            {
                Out = string.Format("HTTP_ERROR :: Exception raised! :: {0}", ex.Message);
                Out += Environment.NewLine + path;
            }
            System.Diagnostics.Debug.WriteLine(Out);
            return Out;
        }
        public async Task<string> HTTP_POSTImageAsync(string path, string convID, byte[] sentData)
        {
            UriBuilder ub = new UriBuilder(baseUri)
            {
                Path = path
            };
            Uri requestUri = ub.Uri;

            string Out = String.Empty;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requestUri);
            request.Method = "POST";
            request.ContinueTimeout = 10000;
            request.ContentType = "application/json";
            request.Accept = "application/json";

            if (username != "")
            {
                request.Headers["x-token"] = token;
                request.Headers["x-username"] = username;
                request.Headers["ConvID"] = convID;
            }

            // = sentData.Length;

            using (System.IO.Stream sendStream = await request.GetRequestStreamAsync())
            {
                sendStream.Write(sentData, 0, sentData.Length);
                sendStream.Dispose();
            }

            try
            {
                WebResponse w = await request.GetResponseAsync();
                Stream responseStream = w.GetResponseStream();

                StreamReader reader = new StreamReader(responseStream);
                Out = reader.ReadToEnd();
            }
            catch (ArgumentException ex)
            {
                Out = string.Format("HTTP_ERROR :: The second HttpWebRequest object has raised an Argument Exception as 'Connection' Property is set to 'Close' :: {0}", ex.Message);
            }
            catch (WebException ex)
            {

                Out = string.Format("HTTP_ERROR :: WebException raised! :: {0}", ex.Message);
                Out += Environment.NewLine + "Content : ";

                Stream ReceiveStreame = ex.Response.GetResponseStream();
                using (StreamReader sr = new StreamReader(ReceiveStreame, Encoding.UTF8))
                {
                    Char[] read = new Char[256];
                    int count = sr.Read(read, 0, 256);

                    while (count > 0)
                    {
                        String str = new String(read, 0, count);
                        Out += str;
                        count = sr.Read(read, 0, 256);

                    }
                }

            }
            catch (Exception ex)
            {
                Out = string.Format("HTTP_ERROR :: Exception raised! :: {0}", ex.Message);
                Out += Environment.NewLine + path;
            }
            System.Diagnostics.Debug.WriteLine(Out);
            return Out;
        }

        async Task<HttpResponseMessage> sendPostRequestAsync(string path, string json)
        {
            UriBuilder ub = new UriBuilder(baseUri)
            {
                Path = path
            };
            Uri requestUri = ub.Uri;

            //replace your Url
            /* dynamic dynamicJson = new ExpandoObject();
             dynamicJson.username = "sureshmit55@gmail.com".ToString();
             dynamicJson.password = "9442921025";

             json = Newtonsoft.Json.JsonConvert.SerializeObject(dynamicJson);
             */

            var httpClient = new System.Net.Http.HttpClient();

            StringContent sC = new StringContent(json);

            //headers
            sC.Headers.Add("x-token", "dx");
            sC.Headers.Add("x-username", username);

            sC.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            // new StringContent(json, Encoding.UTF8, "application/json")
            System.Net.Http.HttpResponseMessage respon = await httpClient.PostAsync(requestUri, sC);
            return respon;
        }
        public async Task<byte[]> POSTrequestBytesAsync(string v, string json)
        {
            HttpResponseMessage respon = await sendPostRequestAsync(v, json);
           // string s = await respon.Content.ReadAsStringAsync(); // debug ;)
            return await respon.Content.ReadAsByteArrayAsync();
        }
        public async Task<string> POSTrequestStringAsync(string v, string json)
        {
            HttpResponseMessage respon = await sendPostRequestAsync(v, json);
            return await respon.Content.ReadAsStringAsync();
        }


        public async Task<string> HTTP_GETAsync(string host, string Url, string Data)
        {
            string Out = String.Empty;
            System.Net.WebRequest req = System.Net.WebRequest.Create(host + Url + (string.IsNullOrEmpty(Data) ? "" : "?" + Data));
            if (token != "")
            {

                req.Headers["x-token"] = token;
                if (username != "")
                {
                    req.Headers["x-username"] = username;
                }
            }

            try
            {
                System.Net.WebResponse resp = await req.GetResponseAsync();
                using (System.IO.Stream stream = resp.GetResponseStream())
                {
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(stream))
                    {
                        Out = sr.ReadToEnd();
                    }
                }
            }

            catch (ArgumentException ex)
            {

                Out = string.Format("HTTP_ERROR :: The second HttpWebRequest object has raised an Argument Exception as 'Connection' Property is set to 'Close' :: {0}", ex.Message);
            }
            /* catch (WebException ex)
             {
                 Out = string.Format("HTTP_ERROR :: WebException raised! :: {0}", ex.Message);
             }*/
            catch (Exception ex)
            {
                Out = string.Format("HTTP_ERROR :: Exception raised! :: {0}", ex.Message);
            }
            System.Diagnostics.Debug.WriteLine(Out);
            return Out;
        }
        public async Task<string> HTTP_PUTAsync(string host, string Url, string Data)
        {
            string Out = String.Empty;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(host + Url);
                request.Method = "PUT";
                request.ContinueTimeout = 100000;
                request.ContentType = "application/json";
                request.Accept = "application/json";

                if (token != "")
                {
                    request.Headers["x-token"] = token;
                }

                byte[] sentData = Encoding.UTF8.GetBytes(Data);
                //      request.ContentLength = sentData.Length;

                using (System.IO.Stream sendStream = await request.GetRequestStreamAsync())
                {
                    sendStream.Write(sentData, 0, sentData.Length);
                    sendStream.Dispose();
                }


                HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();
                using (System.IO.Stream stream = response.GetResponseStream())
                {
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(stream))
                    {
                        return Out = sr.ReadToEnd();
                    }
                }
            }
            catch (ArgumentException ex)
            {

                Out = string.Format("HTTP_ERROR :: The second HttpWebRequest object has raised an Argument Exception as 'Connection' Property is set to 'Close' :: {0}", ex.Message);
            }
            /* catch (WebException ex)
             {
                 Out = string.Format("HTTP_ERROR :: WebException raised! :: {0}", ex.Message);
             }*/
            catch (Exception ex)
            {
                Out = string.Format("HTTP_ERROR :: Exception raised! :: {0}", ex.Message);
            }
            return Out;
        }
        enum DataType
        {
            applicationJson
        }

    }
}

