﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CChat.JSONObjects
{
   /// <summary>
   /// Class to request an element
   /// </summary>
   public class JSONConversationRequestElement
    {
        public String ConvName { get; set; }
        public String ConvID { get; set; }
    }
}
