﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CChat.Objects
{
    public class ErrorMessage
    {
        public static void LogError(Exception ex)
        {
            try
            {
              
                bool save = false;
                string error = GetErrorString(ex);

                System.Diagnostics.Debug.WriteLine("New eror : " + error);

                string errPath = GetTempFile(".err");
                try
                {
                    WriteError(ex);
                    save = true;
                }
                catch
                {
                    File.WriteAllText("C:\\out.txt", "We don't have write acces to write here : " + errPath + Environment.NewLine + error);
                    save = true;
                }
            }
            catch (Exception exe)
            {
                System.Diagnostics.Debug.WriteLine("Warning : complete error !");
                System.Diagnostics.Debug.WriteLine(GetErrorString(exe));
            }
        }
        public static void WriteError(Exception ex)
        {
            String file = GetTempFile(".err");
            File.AppendAllText(file, GetJsonErrorString(ex));
        }
        public static string GetErrorString(Exception ex)
        {
            StringBuilder output = new StringBuilder();

            output.AppendLine("Message : " + ex.Message);
            output.AppendLine("Inner exception" + ex.InnerException);
            output.AppendLine("Source : " + ex.Source);
            output.AppendLine("Stack Trace : " + ex.StackTrace);

            return output.ToString();
        }
        public static string GetJsonErrorString(Exception ex)
        {
            try
            {
                string errorJson = Newtonsoft.Json.JsonConvert.SerializeObject(ex);
                return errorJson;
            }
            catch { return ex.Message; }
        }
        public static string GetTempFile(string extention)
        {
            string directory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "errors");
            if (Directory.Exists(directory) == false) { Directory.CreateDirectory(directory); }

            string name = "current";
            int version = 0;
            while (File.Exists(Path.Combine(directory, name + version + extention)))
            {
                version++;
            }
            return Path.Combine(directory, name + version + extention);
        }
    }
}
