﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CChat.ObjectsHandeler
{
    class FilesManager
    {
        private static int AccountID { get => App.AccountSelected.ID; set => App.AccountSelected.ID = value; }

        private static string GetPath(String element, String FileName)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            path = Path.Combine(path, AccountID.ToString(), element);
            Directory.CreateDirectory(path);

            string filePath = Path.Combine(path, FileName);
            return filePath;
        }

        private static bool AreDifferent(object conv, object local)
        {
            if (conv != local) return true;
            return false;
        }
    }
}

