﻿using CChat.Objects;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;

namespace CChat.ObjectsHandeler
{
    /// <summary>
    /// A class to store and retrive the conversation list
    /// TODO : Add a way to store those data in a database for easier access (faster ????)
    /// Major rewrite to do
    /// TODO : see if using an observable collection would be better
    /// </summary>
    public class ConversationsListManager
    {
        static bool writeLock = false;
        string convPath;
        public ConversationsListManager(string path)
        {
            string dirpath = Path.Combine(path, "conversations");
            Directory.CreateDirectory(dirpath);
            string filePath = Path.Combine(dirpath, "conversationlist");
            convPath = filePath;
            LoadList();
        }
        /// <summary>
        /// cvl : local variable to store temporarly (before saving) the conversation list for faster retrieval
        /// </summary>
        ObservableCollection<ConversationListItem> cvl;


        /// <summary>
        /// Set the conversation list and save it
        /// </summary>
        /// <param name="list"></param>
        public void SetConverationList(ObservableCollection<ConversationListItem> list)
        {
            cvl = list;
            SaveConversationList(cvl.ToArray());
        }


        public void ReplaceConversationList(ConversationListItem[] list)
        {
            cvl.Clear();
            foreach (ConversationListItem cvi in list)
            {
                cvl.Add(cvi);
            }
            SaveConversationList(cvl.ToArray());
        }

        /// <summary>
        /// Add an item to the converation list and save it
        /// </summary>
        /// <param name="conversationElement"></param>
        internal void Add(ConversationListItem conversationElement)
        {
            LoadList(); // List should be loaded first
            cvl.Add(conversationElement);
            SaveConversationList(cvl.ToArray());
        }

        internal void Clear()
        {
            cvl.Clear();
        }

        bool loaded = false; // to store the status
        /// <summary>
        /// load list from storage if not loaded else, do nothing
        /// </summary>
        internal void LoadList()
        {
            if (loaded == false)
            {
                cvl = GetConversationsList();
                loaded = true;
            }
        }

        // store the conversation list, load if needed and save is a change is made
        /// <summary>
        /// Store and retrieve the conversation list
        /// </summary>
        public ObservableCollection<ConversationListItem> ConversationList
        {
            get
            {
                LoadList();
                return cvl;
            }
        }


        /// <summary>
        /// Load conversation list from storage
        /// </summary>
        /// <returns></returns>
        ObservableCollection<ConversationListItem> GetConversationsList()
        {
            if (File.Exists(convPath))
            {
                string json = File.ReadAllText(convPath);
                ObservableCollection<ConversationListItem> list = Newtonsoft.Json.JsonConvert.DeserializeObject<ObservableCollection<ConversationListItem>>(json);
                return list;
            }
            ObservableCollection<ConversationListItem> items = new ObservableCollection<ConversationListItem>();
            return items;

        }

        public bool IsTheConversationListSaved()
        {
            try
            {
                return File.Exists(convPath);
            }
            catch { return false; }
        }

        /// <summary>
        /// Check if the conversation with this name is stored
        /// </summary>
        /// <param name="convID"></param>
        /// <returns></returns>
        internal bool IsStored(string convID)
        {
            foreach (ConversationListItem cv in ConversationList)
            {
                if (cv.ConvID == convID)
                {
                    return true;
                }

            }
            return false;
        }

        internal void SaveConversationList(ConversationListItem[] convs)
        {
            if (writeLock) { }
            writeLock = true;
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(convs);
            File.WriteAllText(convPath, json);
            writeLock = false;
        }
    }
}