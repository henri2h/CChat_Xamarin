﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CChat.Objects;

namespace CChat.ObjectsHandeler
{
    public class ConversationManager
    {
        static bool writeLock = false;
        string convPath;
        public ConversationManager(string path)
        {
            convPath = path;
        }

        string GetPath(string convID)
        {
            string path = Path.Combine(convPath, "conversations", convID);
            Directory.CreateDirectory(path);
            string filePath = Path.Combine(path, "messages");
            return filePath;
        }


        private bool AreDifferent(Conversation conv)
        {
            Conversation local = GetConversation(conv.ConvID);
            if (conv != local) return true;
            return false;
        }



        public void SaveConversation(Conversation conv, string convID)
        {
            while (writeLock) { }
            writeLock = true;
            string path = GetPath(convID);
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(conv);
            File.WriteAllText(path, json);
            writeLock = false;
        }

        public Conversation GetConversation(string convID)
        {
            while (writeLock) { } // here it used as a read lock but anyway
            writeLock = true;
            string path = GetPath(convID);
            if (File.Exists(path))
            {
                string json = File.ReadAllText(path);
                Conversation conv = Newtonsoft.Json.JsonConvert.DeserializeObject<Conversation>(json);
                writeLock = false;
                return conv;
            }
            else
            {
                writeLock = false;
                return null; // if file don't exist
            }
        }

        public bool IsTheConversationSaved(string convID)
        {
            string path = GetPath(convID);
            return File.Exists(path);
        }


    }
}

