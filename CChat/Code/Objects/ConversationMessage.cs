﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CChat.Objects
{
    /// <summary>
    /// A conversation message item
    /// It contains a list of content send with this message
    /// </summary>
    public class ConversationMessage
    {

        public ConversationMessage() { }
        public ConversationMessage(string username, string convID)
        {
            this.Username = username;
            this.ConvID = convID;
            this.SendDate = DateTime.UtcNow.ToString();
            Content = "";
        }

        public string Username { get; set; }
        public string ConvID { get; set; }        
        public int MessageID { get; set; }

        [PrimaryKey, AutoIncrement, JsonIgnore]
        public int LocalMessageID { get; set; }

        public string SendDate { get; set; }
        public string Content { get; set; }
        public dataType DataType { get; set; }

    }

    public enum dataType
    {
        text,
        file,
        image
    }

}

