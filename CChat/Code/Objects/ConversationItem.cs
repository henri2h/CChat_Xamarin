﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CChat.Objects
{
    public class ConversationItem

    {
        public string convID { get; set; } // conversation ID
        public string convName { get; set; } // Conversation Name
        public bool hasImage { get; set; }
        public string imageID { get; set; }
        public string[] usersID { get; set; } // users ID
    }
}
