﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CChat.Objects
{
    /// <summary>
    /// Class to tell the client connector if he should use password or not
    /// </summary>
    public enum ConnectionMethod
    {
        anonymous,
        password

    }
}
