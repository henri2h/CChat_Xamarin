﻿using SQLite;
using System;

namespace CChat.Objects
{
    public class ConversationListItem
    {

        [PrimaryKey, AutoIncrement]
        public int LocalConvID { get; set; }

        public String ConvName { get; set; }

        [Unique]
        public String ConvID { get; set; }
    }
}