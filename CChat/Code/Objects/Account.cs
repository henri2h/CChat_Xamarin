﻿using SQLite;

namespace CChat.Objects
{
    public class Account
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        public string AccountName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        // network
        public string Host { get; set; }
        public int Port { get; set; }
        public bool IsWeb { get; set; }
        public bool Default { get; set; }
    }
}