﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CChat.Objects
{
    public class Conversation
    {
        public string ConvID { get; set; }
        public string[] Users { get; set; }
        public string ConvName { get; set; }
        public ObservableCollection<ConversationMessage> Messages { get; set; }
        public int NumberOfMessage { get; set; }

        public Conversation()
        {
            Messages = new ObservableCollection<ConversationMessage>();
            NumberOfMessage = 0;
            ConvName = "Default name";
            ConvID = "";
            Users = new string[0];
        }
    }
}
