﻿using CChat.Clients;
// CChat
using CChat.Objects;
using CChat.ObjectsHandeler;
using Microsoft.AppCenter.Crashes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading.Tasks;

namespace CChat.ClientConnector
{
    /// <summary>
    /// CLient connector : bridge between the app and the client connector
    /// </summary>
    public class ClientAccess
    {
        bool clientCreated = false;
        public bool Created { get { return clientCreated; } }
        public ConversationsListManager ConvList;
        public ConversationManager ConvMana;
        public ObservableCollection<ConversationListItem> ConversationList { get { return ConvList.ConversationList; } }

        public ObservableCollection<Conversation> Conversation { get; set; }

        List<ConversationMessage> MessagesToSend { get; set; }

        public IClient Client;

        public void CreateConversation(string name, string[] users)
        {
            // create a new client action in the client action list
            Client.CreateConversation(name, users);
        }


        /// <summary>
        /// Get users in a specified conv
        /// </summary>
        /// <param name="convID"></param>
        /// <returns></returns>
        internal IEnumerable<User> GetUsersInConv(string convID)
        {
            return new List<User>() { new User() { username = "no one" } };
        }


        public void AddUser(string convID, string users_to_add)
        {
            Client.AddUser(convID, users_to_add);
        }

        /// <summary>
        /// Destroy the current clientConnector
        /// </summary>
        public void Destroy()
        {
            clientCreated = false;
            App.AccountSelected = null;
        }

        internal async Task<List<ConversationMessage>> FetchFromServerAsync()
        {
            List<ConversationMessage> newMessages = new List<ConversationMessage>();

            // update conversation list
            if (await UpdateConversationListAsync() == false)
            {
                System.Diagnostics.Debug.WriteLine("Update conversation list async false");
            }

            List<ConversationListItem> convListCopy = new List<ConversationListItem>(ConversationList);
            // check in every conversation if new messages have been posted
            foreach (ConversationListItem Conv in convListCopy)
            {

                Conversation convStored = ConvMana.GetConversation(Conv.ConvID);
                // update or download the conversation
                Conversation convUpdated = await UpdateConversationAsync(Conv.ConvID);
                if (convStored != null) // conversation exist
                {
                    int index = convUpdated.Messages.Count - 1; // last index
                    while (index > 0)
                    {
                        /*
                  * todo : OPTIMISATION : enhance the way messages are compared
                  * For example we can start by the last message and go down to the first message and stop once we reached it
                  */
                        // let's see if the message is in convStored
                        // we are going to compare message id
                        bool messageExist = false;
                        foreach (ConversationMessage cMessStored in convStored.Messages)
                        {

                            if (convUpdated.Messages[index].MessageID == cMessStored.MessageID)
                            {
                                messageExist = true;
                            }
                        }

                        if (messageExist == false)
                        {
                            newMessages.Add(convUpdated.Messages[index]);
                        }
                        index--;
                    }

                }
                else
                {

                    Conversation conv = await UpdateConversationAsync(Conv.ConvID);
                    foreach (ConversationMessage cMess in convUpdated.Messages)
                    {
                        newMessages.Add(cMess);
                    }
                }
            }
            // check if new messages have been recieved
            return newMessages;
        }

        /// <summary>
        /// Return the name of a conversation given it's id. /!\ It doesn't update the conversation list
        /// </summary>
        /// <param name="convID"></param>
        /// <returns></returns>
        internal string GetConversationName(string convID)
        {
            foreach(ConversationListItem convItem in ConversationList)
            {
                if(convItem.ConvID == convID)
                {
                    return convItem.ConvName;
                }
            }
            return "Error : No Name";
        }


        // connection part
        /// <summary>
        /// Create a new client and try to update all informations
        /// </summary>
        /// <param name="serverAddress"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="isWeb"></param>
        public ClientAccess(Account account)
        {
            UriBuilder serverAddress = new UriBuilder
            {
                Host = account.Host,
                Port = account.Port
            };

            App.AccountSelected = account;
            String userName = account.UserName;
            String password = account.Password;

            try
            {
                // create the directory and get the path
                string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), account.ID.ToString());
                Directory.CreateDirectory(path);


                // create
                ConvList = new ConversationsListManager(path);
                ConvMana = new ConversationManager(path);
            }
            catch (Exception e)
            {
                // should avert user or do something
                Crashes.TrackError(e);
            }

            account.IsWeb = true; // we only support web currently
            if (account.IsWeb == true)
            {
                Client = new WebClient(userName, serverAddress.Uri);
            }
            else
            {
                // we can't do anything
                System.Diagnostics.Debug.WriteLine("[ Librarie ] : Failure");
            }

            System.Diagnostics.Debug.WriteLine("[ Librarie ] : connection method : " + Client.GetConnectionMethod().ToString());

            var a = TestConectionAsync();

            if (password == null) { password = ""; }
            Client.CurrentUser.username = userName;
            Client.CurrentUser.password = password;

            bool result = Client.Connected;

            System.Diagnostics.Debug.WriteLine("[ Librarie ] : Client connected : " + result);

            Client.Creating = false;
            Client.Created = true;

            clientCreated = true;

            // should populate conversation list
        }

        internal async Task SendImage(string userName, string convID, byte[] image)
        {
            await Client.sendImage(userName, convID, image);
        }

        internal async Task<bool> TestConectionAsync()
        {
            bool connected = await Client.TestConnection();
            System.Diagnostics.Debug.WriteLine("Server is ok : " + connected + Environment.NewLine);
            return Client.Connected;
        }

        internal Task<byte[]> GetImageAsync(string convID, string content)
        {
            return Client.GetConversationImage(content, convID);
        }


        /// <summary>
        /// Set the conversation name
        /// </summary>
        /// <param name="ConvData"></param>
        /// <returns></returns>
        public Task SetConversationName(ConversationListItem ConvData)
        {
            return SetConversationName(ConvData.ConvID, ConvData.ConvName);
        }

        /// <summary>
        /// Set the conversation name
        /// </summary>
        /// <param name="convID"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public Task SetConversationName(string convID, string convName)
        {
            return Client.ChangeConversationName(convID, convName);
        }


        /// <summary>
        /// Return true if client is connected to server and ready to accept requests
        /// </summary>
        public bool ClientIsConnected
        {
            get { return Client.Connected; }
        }

        /// <summary>
        /// Return true if user is connected and server responding
        /// </summary>
        /// <returns></returns>
        public async Task<bool> CanUseClientAsync()
        {
            return await Client.CanUserUseConnection();
        }


        public Account _Account { get; set; }


        async void UpdateAllAsync()
        {
            await UpdateConversationListAsync();


            foreach (ConversationListItem Conv in ConversationList)
            {
                Conversation conv = await UpdateConversationAsync(Conv.ConvID);
            }

        }


        /// <summary>
        /// Update conversation list
        /// </summary>
        /// <param name="convID"></param>
        /// <returns></returns>
        public async Task<Conversation> UpdateConversationAsync(String convID)
        {
            try
            {
                Conversation conv = await Client.GetConversation(convID);
                ConvMana.SaveConversation(conv, convID);
                return conv;
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
                return null;
            }
        }

        /// <summary>
        /// Update conversation list (fetch list from server)
        /// </summary>
        public async Task<bool> ForceUpdateConversationListAsync()
        {
            return await UpdateConversationListAsync();
        }

        /// <summary>
        /// Download the new conversation list and save it
        /// </summary>
        /// <returns></returns>
        public async Task<bool> UpdateConversationListAsync()
        {
            try
            {
                ConversationListItem[] convs = await Client.ListConversations();
                // save conversation list
                ConvList.ReplaceConversationList(convs);
                return true;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                Crashes.TrackError(e);
                return false;
            }
        }

        /// <summary>
        /// Send a message, send true if success or false if send to message to send list
        /// </summary>
        /// <param name="tcpConvMess"></param>
        /// <returns></returns>
        public async Task<bool> SendConversationMessage(ConversationMessage tcpConvMess)
        {
            if (ClientIsConnected)
            {
                await Client.SendConversationMessage(tcpConvMess);
                return true;
            }
            else
            {
                // to do manage this list
                MessagesToSend.Add(tcpConvMess);
                return false;
            }
        }
        /// <summary>
        /// To easely send a message
        /// </summary>
        /// <param name="text"></param>
        /// <param name="convID"></param>
        /// <returns></returns>
        async Task<bool> SendConversationMessage(string text, string convID)
        {
            ConversationMessage nMess = new ConversationMessage(_Account.UserName, convID);
            nMess.Content = text;
            nMess.SendDate = DateTime.Now.ToString();
            return await SendConversationMessage(nMess);
        }

        public Conversation GetConversation(string convID)
        {
            return ConvMana.GetConversation(convID);
        }

        // file downloader

        List<FileToDownload> fileToDownload = new List<FileToDownload>();

        public async Task GetItemsFromFileAsync() { await GetItemsAsync(0); }
        public async Task GetItemsFromServerAsync() { await GetItemsAsync(1); }
        public async Task ForceGetItemsFromServerAsync() { await GetItemsAsync(2); }

        private async Task GetItemsAsync(int dMode)
        {
            List<FileToDownload> toReload = new List<FileToDownload>();
            List<Task<bool>> toWait = new List<Task<bool>>();


            foreach (FileToDownload d in fileToDownload)
            {
                try
                {
                    byte[] imageBytes = await GetFileAsync(d.FileName, d.ConvID, dMode);

                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("Could not download image : " + ex.Message);
                    toReload.Add(d);
                }
            }

            foreach (Task<bool> task in toWait)
            {
                await task;
            }

            fileToDownload = toReload;
        }

        internal void Clear()
        {
            fileToDownload.Clear();
        }

        public async Task<byte[]> GetFileAsync(string fileName, string convID, int dMode)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            path = Path.Combine(path, App.AccountSelected.ID.ToString(), "conversations", convID);

            string filePath = Path.Combine(path, fileName);

            byte[] imageBytes;
            if (File.Exists(filePath) == false || dMode == 2)
            {
                if (dMode > 0)
                {
                    imageBytes = await Client.GetConversationImage(fileName, convID);
                    Directory.CreateDirectory(path);
                    File.WriteAllBytes(filePath, imageBytes);

                    System.Diagnostics.Debug.WriteLine("New image downloaded : " + fileName + ", convID : " + convID);
                }
                else return null;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("File : " + fileName + " already exist");
                imageBytes = File.ReadAllBytes(filePath);
            }

            return imageBytes;
        }

    }
}
