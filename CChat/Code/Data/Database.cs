﻿using CChat.Objects;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace CChat.Code.Data
{
    public class Database
    {
        readonly SQLiteAsyncConnection _database;

        public Database()
        {
            string dbPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Database.db3");
            _database = new SQLiteAsyncConnection(dbPath);
            _database.CreateTableAsync<Account>().Wait();
            _database.CreateTableAsync<ConversationListItem>().Wait();
            _database.CreateTableAsync<ConversationMessage>().Wait();
        }

        public Task<List<Account>> GetAccountsAsync()
        {
            return _database.Table<Account>().ToListAsync();
        }


        public Task<List<Account>> GetDefaultAccountsAsync()
        {
            return _database.Table<Account>()
                            .Where(i => i.Default == true).ToListAsync();
        }
        public Task<Account> GetDefaultAccountAsync()
        {
            return _database.Table<Account>()
                            .Where(i => i.Default == true)
                            .FirstOrDefaultAsync();
        }

        public Task<Account> GetAccountAsync(int id)
        {
            return _database.Table<Account>()
                            .Where(i => i.ID == id)
                            .FirstOrDefaultAsync();
        }

        /// <summary>
        /// Add an account to the database as default, setting the other as non default. If user already exist, set it as default
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public Task<int> AddDefaultAccountAsync(Account account)
        {
            SetAllAccountToNonDefault();
            account.Default = true;
            if (account.ID != 0)
            {
                return _database.UpdateAsync(account);
            }
            else
            {
                return _database.InsertAsync(account);
            }
        }

        public async void SetAllAccountToNonDefault()
        {
            foreach (Account ac in await GetDefaultAccountsAsync())
            {
                ac.Default = false;
                await _database.UpdateAsync(ac);
            }
        }

        public async Task<int> SetAccountAsDefaultAsync(Account account)
        {
            return await AddDefaultAccountAsync(account); // as the account should already exist, we just have to update it
        }

        public Task<int> DeleteAccountAsync(Account account)
        {
            return _database.DeleteAsync(account);
        }









        // messages and conversations
        public Task<int> AddConversationListItem(ConversationListItem account)
        {
            SetAllAccountToNonDefault();
            if (account.LocalConvID != 0)
            {
                return _database.UpdateAsync(account);
            }
            else
            {
                return _database.InsertAsync(account);
            }
        }



    }
}
